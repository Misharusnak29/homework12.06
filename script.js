//Теоретичні питання

/* 
1.Метод об'єкту - це функція, яка є властивістю об'єкта. Методи об'єкта зазвичай використовуються для виконання дій або операцій, які мають відношення до самого об'єкта. Вони можуть працювати з властивостями об'єкта або змінювати їх.

2.Тип даних властивості об'єкта може бути будь-яким, включаючи примітивні типи даних (рядок, число, булевий), масиви, функції (методи), інші об'єкти тощо. Властивість об'єкта може також містити значення null або undefined.

3.Посилальний тип даних означає, що змінна зберігає не саме значення, а посилання на місце в пам'яті, де це значення зберігається. У випадку об'єктів це означає, що коли ви присвоюєте об'єкт іншій змінній, обидві змінні вказують на один і той самий об'єкт у пам'яті. Зміни, внесені через одну змінну, відобразяться і на іншій.
*/ 

//Практичні завдання

// 1. const product = {
//     name: "Example Product",
//     price: 100,
//     discount: 10,
//     getFinalPrice: function() {
//       return this.price - (this.price * this.discount / 100);
//     }
//   };
  
//   console.log(product.getFinalPrice());
  

// 2. function greeting(user) {
//   return `Привіт, мені ${user.age} років.`;
// }

// const userName = prompt("Введіть своє ім'я:");
// const userAge = prompt("Введіть свій вік:");

// const user = {
//   name: userName,
//   age: userAge
// };

// alert(greeting(user));

// 3. function deepClone(obj) {
//     if (obj === null || typeof obj !== 'object') {
//       return obj;
//     }
  
//     if (Array.isArray(obj)) {
//       const arrCopy = [];
//       for (let i = 0; i < obj.length; i++) {
//         arrCopy[i] = deepClone(obj[i]);
//       }
//       return arrCopy;
//     }
  
//     const objCopy = {};
//     for (const key in obj) {
//       if (obj.hasOwnProperty(key)) {
//         objCopy[key] = deepClone(obj[key]);
//       }
//     }
//     return objCopy;
//   }
  
//   const original = {
//     name: "Original",
//     details: {
//       age: 30,
//       hobbies: ["reading", "gaming"]
//     }
//   };
  
//   const clone = deepClone(original);
//   clone.details.hobbies.push("swimming");
  
//   console.log(original);
//   console.log(clone);
  